/**
 For simple applications, you might define all of your views in this file.
 For more complex applications, you might choose to separate these kind definitions
 into multiple files under this folder.
 */

var kind = require('enyo/kind'),
    FittableRows = require('layout/FittableRows'),
    Panels = require('layout/Panels'),
    CollapsingArranger = require('layout/CollapsingArranger'),
    DockRightArranger = require('layout/DockRightArranger'),
    FittableColumns = require('layout/FittableColumns');

module.exports = kind({
    name: 'pl.MainView',
    kind: FittableRows,
    classes: 'enyo-fit enyo-unselectable',
    handlers: {},
    components: [
      { name: 'FirstPanel', kind: Panels, arrangerKind: CollapsingArranger, fit: true, realtimeFit: true, components: [
        { name: 'Panel1', content: 'Panel 1', classes: 'panel panel1' },
        { name: 'SecondPanel', kind: Panels, arrangerKind: CollapsingArranger, fit: true, realtimeFit: true, components: [
          { name: 'Panel2', content: 'Panel 2', fit: true, classes: 'panel panel2', components: [
            { content: 'hello', style: 'float: right;'},
            { content: 'Hello' }
          ] },
          { name: 'Panel3', content: 'Panel 3', classes: 'panel panel3' },
          { name: 'Panel4', content: 'Panel 4', classes: 'panel panel4' }
        ]}
      ]}
    ],
    rendered: function () {
      this.inherited(arguments);
      this.$.FirstPanel.setIndex(0);
    },
    handler: function (inEvent, inSender ) {
      switch (inEvent.type) {
        case 'dragstart':
        this.$.Panel3.draggable = true;
          break;
        case 'dragfinish':
        this.$.Panel3.draggable = false;
          break;
      }
    }
});
