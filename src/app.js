var kind = require('enyo/kind'),
    Application = require('enyo/Application'),
    plMainView = require('./views/MainView.js');

    var app = module.exports = kind({
      name: 'pl.Application',
      kind: Application,
      view: plMainView
  });