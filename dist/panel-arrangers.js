(function (scope, bundled) {
	
	var   enyo     = scope.enyo || (scope.enyo = {})
		, manifest = enyo.__manifest__ || (defineProperty(enyo, '__manifest__', {value: {}}) && enyo.__manifest__)
		, exported = enyo.__exported__ || (defineProperty(enyo, '__exported__', {value: {}}) && enyo.__exported__)
		, require  = enyo.require || (defineProperty(enyo, 'require', {value: enyoRequire}) && enyo.require)
		, local    = bundled()
		, entries;

	// below is where the generated entries list will be assigned if there is one
	entries = ['index'];


	if (local) {
		Object.keys(local).forEach(function (name) {
			var value = local[name];
			if (manifest.hasOwnProperty(name)) {
				if (!value || !(value instanceof Array)) return;
			}
			manifest[name] = value;
		});
	}

	function defineProperty (o, p, d) {
		if (Object.defineProperty) return Object.defineProperty(o, p, d);
		o[p] = d.value;
		return o;
	}
	
	function enyoRequire (target) {
		if (!target || typeof target != 'string') return undefined;
		if (exported.hasOwnProperty(target))      return exported[target];
		var   request = enyo.request
			, entry   = manifest[target]
			, exec
			, map
			, ctx
			, reqs
			, reqr;
		if (!entry) throw new Error('Could not find module "' + target + '"');
		if (!(entry instanceof Array)) {
			if (typeof entry == 'object' && (entry.source || entry.style)) {
				throw new Error('Attempt to require an asynchronous module "' + target + '"');
			} else if (typeof entry == 'string') {
				throw new Error('Attempt to require a bundle entry "' + target + '"');
			} else {
				throw new Error('The shared module manifest has been corrupted, the module is invalid "' + target + '"');
			}
		}
		exec = entry[0];
		map  = entry[1];
		if (typeof exec != 'function') throw new Error('The shared module manifest has been corrupted, the module is invalid "' + target + '"');
		ctx  = {exports: {}};
		if (request) {
			if (map) {
				reqs = function (name) {
					return request(map.hasOwnProperty(name) ? map[name] : name);
				};
				defineProperty(reqs, 'isRequest', {value: request.isRequest});
			} else reqs = request;
		}
		reqr = !map ? require : function (name) {
			return require(map.hasOwnProperty(name) ? map[name] : name);
		};
		exec(
			ctx,
			ctx.exports,
			scope,
			reqr,
			reqs
		);
		return exported[target] = ctx.exports;
	}

	// in occassions where requests api are being used, below this comment that implementation will
	// be injected
	

	// if there are entries go ahead and execute them
	if (entries && entries.forEach) entries.forEach(function (name) { require(name); });
})(this, function () {
	// this allows us to protect the scope of the modules from the wrapper/env code
	return {'src/views/MainView':[function (module,exports,global,require,request){
/**
 For simple applications, you might define all of your views in this file.
 For more complex applications, you might choose to separate these kind definitions
 into multiple files under this folder.
 */

var kind = require('enyo/kind'),
    FittableRows = require('layout/FittableRows'),
    Panels = require('layout/Panels'),
    CollapsingArranger = require('layout/CollapsingArranger'),
    DockRightArranger = require('layout/DockRightArranger'),
    FittableColumns = require('layout/FittableColumns');

module.exports = kind({
    name: 'pl.MainView',
    kind: FittableRows,
    classes: 'enyo-fit enyo-unselectable',
    handlers: {},
    components: [
      { name: 'FirstPanel', kind: Panels, arrangerKind: CollapsingArranger, fit: true, realtimeFit: true, components: [
        { name: 'Panel1', content: 'Panel 1', classes: 'panel panel1' },
        { name: 'SecondPanel', kind: Panels, arrangerKind: CollapsingArranger, fit: true, realtimeFit: true, components: [
          { name: 'Panel2', content: 'Panel 2', fit: true, classes: 'panel panel2', components: [
            { content: 'hello', style: 'float: right;'},
            { content: 'Hello' }
          ] },
          { name: 'Panel3', content: 'Panel 3', classes: 'panel panel3' },
          { name: 'Panel4', content: 'Panel 4', classes: 'panel panel4' }
        ]}
      ]}
    ],
    rendered: function () {
      this.inherited(arguments);
      this.$.FirstPanel.setIndex(0);
    },
    handler: function (inEvent, inSender ) {
      switch (inEvent.type) {
        case 'dragstart':
        this.$.Panel3.draggable = true;
          break;
        case 'dragfinish':
        this.$.Panel3.draggable = false;
          break;
      }
    }
});

}],'src/app':[function (module,exports,global,require,request){
var kind = require('enyo/kind'),
    Application = require('enyo/Application'),
    plMainView = require('./views/MainView.js');

    var app = module.exports = kind({
      name: 'pl.Application',
      kind: Application,
      view: plMainView
  });
},{'./views/MainView.js':'src/views/MainView'}],'index':[function (module,exports,global,require,request){
// This is the default "main" file, specified from the root package.json file
// The ready function is excuted when the DOM is ready for usage.

var ready = require('enyo/ready'),
		App = require('./src/app.js');

ready(function() {
	new App().renderInto(document.body);
});

},{'./src/app.js':'src/app'}]
	};

});
//# sourceMappingURL=panel-arrangers.js.map